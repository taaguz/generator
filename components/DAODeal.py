from components.SQLFunctions import *
import sys
sys.path.insert(0, './datagenerator/')
from components.RandomDealData import *

class DAODeal:
    def storeDataFRomDealAlgo(self):
        count = 0
        while count<2000 :
            dealData = RandomDealData()
            instrumentList = dealData.createInstrumentList()
            res =json.loads(dealData.createRandomData(instrumentList))
            instrumentName = res['instrumentName']
            partyName = res['cpty']
            price = res['price']
            deal_type = res['type']
            quantity = res['quantity']
            deal_time = res['time']
            print(instrumentName, partyName,price,deal_type,quantity,deal_time )
            # sqlFunction = SQLFunctions()
            # sqlFunction.addInstruments(instrumentName)
            # sqlFunction.addCounterparty(partyName)
            # sqlFunction.addType(deal_type)
            # sqlFunction.addDeal(instrumentName,partyName,deal_type,price,quantity,deal_time)
            count+=1
            yield str(res)

    def getDealData(self):
        sqlFunction = SQLFunctions()
        res = sqlFunction.getDealData()
        print(json.dumps(res))
        return (json.dumps(res))

    def getAvgBuyPrice(self):
        sql = SQLFunctions()
        res = sql.averageBuyInstrumentPrice()
        print(json.dumps(res))
        return (json.dumps(res)) 

    def getAvgSellPrice(self):
        sql = SQLFunctions()
        res = sql.averageSellInstrumentPrice()
        print(json.dumps(res))
        return (json.dumps(res)) 


dao = DAODeal()
""" sql = SQLFunctions()
sql.averageBuyInstrumentPrice()
sql.averageSellInstrumentPrice()
dao.storeDataFRomDealAlgo()
dao.getDealData()
 """
dao.getAvgBuyPrice()
dao.getAvgSellPrice()