import mysql.connector
from datetime import datetime


class SQLFunctions:

    def __init__(self):
        #self.mydb = mysql.connector.connect(host='192.168.99.100', database='investmentServicesGroup12', user='root', password='ppp')
        self.mydb = mysql.connector.connect(host='mysql.please-work.svc.cluster.local', database='investmentServicesGroup12', user='user485', password='ghDFRq4Fe5aRSNUC')
        self.mycursor = self.mydb.cursor()

    # ---------------------------------------------
    # Insert


    def addDeal(self, instrument, cpty, deal_type, price, quantity, deal_time):
        name = (instrument,)
        selectInstrumentID = "select instrument_id from instruments where instrument_name = (%s)"
        self.mycursor.execute(selectInstrumentID, name)
        instrumentId = 0
        result_set = self.mycursor.fetchall()
        for i in result_set:
            instrumentId = i[0]
        # print(instrumentId)

        partyName = (cpty,)
        query = "select cpty_id from counterparties where cpty_name = (%s)"
        self.mycursor.execute(query, partyName)
        cpId = 0
        result_set = self.mycursor.fetchall()
        for i in result_set:
            cpId = i[0]
        # print(cpId)

        val = (instrumentId, cpId, deal_type, price, quantity, deal_time,)
        add_deal = "insert into deals (instrument, cpty, deal_type, price, quantity, deal_time) VALUES (%s, %s, %s, %s, %s, %s)"
        self.mycursor.execute(add_deal, val)
        self.mydb.commit()
        # print("Finally done")

    # -------------------------------------------------

    def getDealData(self):
        query = "select instrument_name, cpty_name, price, type_name, quantity, deal_time from instruments i, counterparties c, deals d where i.instrument_id = d.instrument and c.cpty_id = d.cpty"
        self.mycursor.execute(query)
        result_set = self.mycursor.fetchall()
        return result_set

    # -------------------------------------------------

    def averageBuyInstrumentPrice(self):
        instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                       "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
        buyList = []
        for instrumentName in instruments:
            val = (instrumentName,)
            query = "select i.instrument_name, avg(price) as BuyPrice from instruments i, deals d where i.instrument_name = (%s) and d.deal_type='B' AND i.instrument_id = d.instrument "
            self.mycursor.execute(query, val)
            result_set = self.mycursor.fetchall()
            # print(result_set)
            buyList.append(result_set)
        # print(buyList)
        return buyList

    def averageSellInstrumentPrice(self):
        instruments = ("Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
                       "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic")
        sellList = []
        for instrumentName in instruments:
            val = (instrumentName,)
            query = "select i.instrument_name, avg(price) as SellPrice from instruments i, deals d where i.instrument_name = (%s) and d.deal_type='S' AND i.instrument_id = d.instrument "
            self.mycursor.execute(query, val)
            result_set = self.mycursor.fetchall()
            sellList.append(result_set)
        # print(sellList)
        return sellList

    def get_user(self, user_name):
        val = (user_name,)
        get_user = 'SELECT * FROM credentials WHERE user_name = %s'
        self.mycursor.execute(get_user, val)
        result_set = self.mycursor.fetchall()
        return result_set

    def get_all_users(self):
        get_all_users = 'SELECT * FROM credentials'
        self.mycursor.execute(get_all_users)
        result_set = self.mycursor.fetchall()
        self.mydb.commit()

        return result_set

    def add_user(self, user_name, password, role_num):
        val = (user_name, password, role_num, )
        add_user = 'INSERT INTO credentials (user_name, password, role_num) VALUES (%s, %s, %s)'

        result_set = self.mycursor.execute(add_user, val)
        self.mydb.commit()

        return result_set

    def delete_user(self, user_name):
        val = (user_name, )
        delete_user = 'DELETE FROM credentials WHERE user_name = %s'

        result_set = self.mycursor.execute(delete_user, val)
        self.mydb.commit()

        return result_set

    def update_password(self, password, user_name):
        val = (password, user_name, )
        update_password = 'UPDATE credentials SET password = %s WHERE user_name = %s'
        #self.mycursor.execute(update_password, val)
        result_set = self.mycursor.execute(update_password, val)
        self.mydb.commit()
        return result_set
        
    def getEffectiveProfit(self):
        startDay = 1
        endDay = 10
        i = 0
        queryCount = "select count(*) from deals"
        self.mycursor.execute(queryCount)
        countRow = 0
        result_set = self.mycursor.fetchall()
        effectiveProfit = []
        for r in result_set:
            countRow = int(r[0])

        chunk = countRow/10
        while i < chunk:
            val = (startDay, endDay,)
            qbuyAvg = "select avg(price) from deals where deal_type ='B' and deal_id >= (%s) and deal_id < (%s)"
            qsellAvg = "select avg(price) from deals where deal_type='S' and deal_id >= (%s) and deal_id < (%s)"
            qsumSell = "select sum(quantity) from deals where deal_type = 'S' and deal_id >= (%s) and deal_id < (%s)"

            self.mycursor.execute(qbuyAvg, val)
            avgBuyPrice = 0
            buyPrice = self.mycursor.fetchall()
            for b in buyPrice:
                avgBuyPrice = b[0]

            self.mycursor.execute(qsellAvg, val)
            avgSellPrice = 0
            sellPrice = self.mycursor.fetchall()
            for s in sellPrice:
                avgSellPrice = s[0]

            self.mycursor.execute(qsumSell, val)
            sumSellShares = 0
            sumSell = self.mycursor.fetchall()
            for ss in sumSell:
                sumSellShares = int(ss[0])
            profitDay = (avgSellPrice-avgBuyPrice)
            effectiveProfit.append(profitDay*sumSellShares)

            startDay += 1
            endDay += 1
            i += 1
        return effectiveProfit

