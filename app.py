from flask import Flask, Response, jsonify, request
from flask_cors import cross_origin

from components.DAODeal import DAODeal, SQLFunctions

app = Flask(__name__)

@app.route('/streamdeals')
def dealAlgo():
    # dealData = RandomDealData()
    # instrumentList = dealData.createInstrumentList()
    # res = dealData.createRandomData(instrumentList)
    dao_deal = DAODeal()
    return Response(dao_deal.storeDataFRomDealAlgo(), mimetype="text/event-stream")


sql_functions = SQLFunctions()

#returns every user in table
@app.route('/users')
@cross_origin()
def get_all_users():
    return jsonify(sql_functions.get_all_users())

#works, tested
@app.route('/user', methods=['POST'])
@cross_origin()
def create_user():
    json = request.get_json()
    name = json['username']
    password = json['password']
    role = json['role']

    if role == 'trader':
        role_num = 2
    elif role == 'security':
        role_num = 1

    return jsonify(sql_functions.add_user(user_name=name, password=password, role_num=role_num))

#checked, working
@app.route('/user', methods=['DELETE'])
@cross_origin()
def delete_user():
    json = request.get_json()
    name = json['username']
    sql_functions.delete_user(name)
    return "OK"

#not working
@app.route('/user', methods=['PUT'])
@cross_origin()
def update_password():
    json = request.get_json()
    name = json['username']
    password = json['password']
    sql_functions.update_password(password, name)
    return "OK"

@app.route('/trader/average-buy')
def getAverageBuyPrice():
    return jsonify(sql_functions.averageBuyInstrumentPrice())

@app.route('/trader/average-sell')
def getAverageSellPrice():
    return jsonify(sql_functions.averageSellInstrumentPrice())

@app.route('/trader/effectiveProfit')
def getEffectiveProfitLoss():
    return jsonify(sql_functions.getEffectiveProfit)

if __name__ == '__main__':
    app.run(port=8080, host=('0.0.0.0'), threaded=True)
